import VueRouter from 'vue-router';
import List from './pages/List.vue';
import Detail from './pages/Detail.vue';
import Register from './pages/Register.vue';
import Update from './pages/Update.vue';

const router = new VueRouter({
    routes: [
        {
            path: '/',
            component: List
        },
        {
            path: '/user/:id',
            component: Detail
        },
        {
            path: '/register',
            component: Register
        },
        {
            path: '/user/:id/update',
            component: Update
        }
    ]
});

export default router;