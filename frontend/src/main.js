import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import router from './routes';

import JwPagination from 'jw-vue-pagination';

Vue.use(VueRouter);

Vue.config.productionTip = false
Vue.component('jw-pagination', JwPagination);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
