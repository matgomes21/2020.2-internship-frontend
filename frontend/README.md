# Mateus Gomes do Nascimento

## Design UI/UX
[Design das página](https://www.figma.com/file/a66se6cErVYSPsL2hjMVfA/Rua-Dois-Est%C3%A1gio-Front-End?node-id=0%3A1) desenvolvivo no [Figma](https://www.figma.com/).

## Tecnologias

- [NodeJS](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/)
- [VueJS CLI](https://cli.vuejs.org/)
- [axios](https://www.npmjs.com/package/axios)
- [Vue Router](https://router.vuejs.org/)

## Telas

<img src="./.github/Landing.png" />
<img src="./.github/Detail.png" />
<img src="./.github/Form.png" />

## Como Rodar

Clone este repositório
``` bash
git clone https://gitlab.com/matgomes21/2020.2-internship-frontend.git
```

Entre na pasta do projeto
```bash
cd 2020.2-internship-frontend 
cd frontend
```

Instale as dependências (npm/yarn)
```bash
yarn install
```

Inicie a aplicação
```bash
yarn serve
```

Acesse o endereço informado no terminal para ter acesso à aplicação.

## Esquema de pastas

Rotas
```
src/routes.js
```

Componentes páginas
```
src/pages/
```

Demais componentes
```
src/components/
```

## Pontos importantes do código

### Conexão com a API

A conexão com a API foi feita utiliando a biblioteca axios e requisições HTTP.

Ex:

```javascript
await axios.get(`http://5f88a118a8a2b5001641ef57.mockapi.io/ruadois/api/users/${this.id}`).then(response => {
      this.user = response.data;
      console.log(this.user);
    });
```
```src/pages/Detail.vue linha 49```

Requisição get de um usuário específico utilizando a rota /:id 

### Responsividade

Responsividade feita na Landing Page, mudando a exibição de duas colunas para uma.

```css
@media(max-width: 1000px) {
    .user-list {
        display: flex;
        flex-direction: column;
    }
}
```

```src/pages/List.vue linha 102```

### Validação

Validação no formulário de cadastro, emitindo um alerta caso algum input esteja em branco ou realizando uma requisição HTTP Post caso todos os campos estejam preenchidos.

```javascript
if (
    this.userInput.name === "" ||
    this.userInput.avatar === "" ||
    this.userInput.email === "" ||
    this.userInput.username === "" ||
    this.userInput.description === "" ||
    this.userInput.address === "" ||
    this.userInput.about === ""
) {
    alert("Preencha todos os campos!")
} else {
    await axios.post(
        "http://5f88a118a8a2b5001641ef57.mockapi.io/ruadois/api/users",
        this.userInput
    );
    window.location.href = "/";
}
```

```src/pages/Register.vue linha 49```